import '../styles/Signup.css';
import EmailInformation from '../components/EmailInformation';
import { useState } from 'react';
import { } from 'react-bootstrap';


import GeneralInformation from '../components/GeneralInformation';

export default function Signup(){
	let [registered, setRegistered] = useState({
		steps:1,
		firstname:"",
		lastname:"",
		lotNumber:"",
		barangay:"",
		city:"",
		province:"",
		country:"",
		gender:"",
		birthday:"",
		email:"",
		password:"",
		password2: ""		
	})



	const PrevStep = () => {
		const { steps } = registered
		return setRegistered(prevValue => {
			return { ...prevValue,  steps: steps - 1 }
		})
	}

	const NextStep = () => {
		const { steps } = registered
		return setRegistered(prevValue => {
			return { ...prevValue,  steps: steps + 1 }
		})
	}
	const HandleChange = (e) => {	
		const { value, name } = e.target
		return setRegistered(prevValue => {
			return { ...prevValue, [name]:value }
		})
	
	} 

	const { steps } = registered;
	const {firstname, lastname, gender, birthday, email, password, password2, lotNumber, barangay, city, province, country} = registered;
	const values = {firstname, lastname, gender, birthday, email, password, password2, lotNumber, barangay, city, province, country}


	switch(steps){
		case 1:
			return(
				<GeneralInformation
					nextStep = {NextStep}
					handleChange = {HandleChange}
					values = { values }
				/>	
			)
		case 2:
			return(
				<EmailInformation
					nextStep = { NextStep }
					prevStep = { PrevStep }
					handleChange = { HandleChange }
					values = { values }
				/>
			)
		default:
			// do nothing
	} 
}