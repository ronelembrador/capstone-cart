import '../styles/About.css';
import { Fragment } from 'react';
import { Row, Col, Image } from 'react-bootstrap';

export default function About(){
	const data = [
		{
			title:"Vision",
			description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."
		},
		{
			title:"Mission",
			description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."
		},
	]

	const aboutContent = data.map((value, index) => {
		return <div key={index}>
				<p className="mission-vission__title">{value.title}</p>
					<p className="vision-mission-description">{value.description}</p>
				</div>
	})

	return <Fragment>
		<div className="about-container">
			<Row  className="about-box">
				<Col md="6">
					<div>
						<Image src="https://images.unsplash.com/uploads/1412443967017d95e2723/2919e13b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" className="about-img"/>
					
					</div>
				</Col>
				<Col md="6">
						<div>
							<div>
								<p className="about-title">Farmers<span className="about-title-country">PH</span>.</p>
								<p className="about-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
								</p>
							</div>
						</div>
				</Col>
			</Row>
			<div className="vision-mission-box">
				{aboutContent}
			</div>
		</div>
	</Fragment>
}