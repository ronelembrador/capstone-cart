import '../styles/Profile.css';
import { Fragment, useState, useEffect } from 'react';
import Container from '../components/Container';

export default function Profile(){
	const [ address, setAddress ] = useState([]);
	const [ fullName, setFullName] = useState([]);
	const [ info, setInfo ] = useState([]);

	const { id, birthday, email, gender } = info;
	useEffect(() => {
	    let token = localStorage.getItem('token');
	    fetch('https://gentle-dusk-18521.herokuapp.com/user/profile',{
	      method: 'GET',
	      headers: { Authorization: `Bearer ${token}`}
	    })
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setAddress(data.address.map((address,index) => {
				return <div key={index} className="address-box">
					<p className="personal-title">Address:</p>
					<span>{address.lotNumber} {address.barangay} {address.city} {address.province} {address.country}</span>
				</div>
			}))
			setFullName(data.fullName.map((name,index) => {
				return <div key={index} className="address-box">
					<p className="personal-title">Name: </p>
					<span>{name.givenName} {name.familyName}</span>
				</div>
			}))
			setInfo(() => {
				return <div>
					<div  className="address-box">
						<p className="personal-title">Email:</p>
						<span>{data.email}</span>
					</div>
					<div  className="address-box">
						<p className="personal-title">Gender:</p>
						<span>{data.gender}</span>
					</div>
					<div  className="address-box">
						<p className="personal-title">Birthday:</p>
						<span>{data.birthDate}</span>
					</div>
				</div>
			})			

		});
    },[])

	return <Fragment>	
		<Container>
			<div className="manage-account__box">
			<p className="manage-pesonal__header">Manage My Account</p>
				<p className="manage-pesonal__sub-header">Personal Details</p>
				{fullName}
				{info}
				{address}
			</div>
		</Container>
	</Fragment>	
}
