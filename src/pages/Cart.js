import '../styles/Cart.css'
import {Fragment, React, useState} from 'react'
import {Container, Row, Col, Button} from 'react-bootstrap'
import CartLayout from '../components/CartLayout'



export default function Cart(props) {

	let [isLoaded, setIsLoaded] = useState(2);

	const cartDetails = [
		{
			name: 'Cabbage',
			price: 588,
			qty: 0
		},
		{
			name: 'Eggplant',
			price: 588,
			qty: 3
		},
		{
			name: 'Be Successful',
			price: 588,
			qty: 2
		},
	]

	let Cart = cartDetails.map((item,index) => {
		return <Col key={index}> 
			<CartLayout id={index} name={item.name} price={item.price} qty={item.qty} /> 
		</Col>

	})

	function loadMore(){
		setIsLoaded(isLoaded += 2)
	}

	function loadLess(){
		setIsLoaded(isLoaded -= 2)
	}

	Cart = Cart.slice(0, isLoaded);

	return (

		<Fragment>
{/*			<Container className="main">*/}
				<div className="pt-2 main center">
					<div className="cartHead">
						Cart Details
					</div>
				</div>
				<div className="main2 px-0 mx-auto">
				{Cart}
					<div className="buttons">
					{isLoaded <= Cart.length? <Button variant="primary" onClick={loadMore} className="button-loadMore2" size="sm">Load More</Button> : <Button variant="danger" onClick={loadLess} className="button-loadless2" size="sm">show less</Button>}
					{Cart.length > 0 ? <Button className="button-checkout" size="sm">Proceed to Checkout</Button> : <Button className="button-checkout" size="sm" disabled>Proceed to Checkout</Button>}
					</div>
				</div>
			{/*</Container>*/}
		</Fragment>

	)
}