import '../styles/ForgotPassword.css';
import { Fragment, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faEye  } from '@fortawesome/free-solid-svg-icons'; 
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ForgotPassword(){
	let disable = true;
	const [passwordVisible, setIsPasswordVisible] = useState(true);
	const [passwordType, setPasswordType] = useState('password');
	const [userInfo, setUserInfo] = useState({
		email:'',
		birthDate:'',
		password:''
	})

	const history = useNavigate();
	const { email, birthDate, password } = userInfo;
		if(email && birthDate && password) disable = false
		else disable = true
	const makePasswordVisible = () => {
		if(passwordVisible){
			setPasswordType('text')
			setIsPasswordVisible(false)
		}
		else{
			setPasswordType('password')	
			setIsPasswordVisible(true)
		} 
	}
	const onChangeHandler = (e) => {
		const {name, value} = e.target;
		return setUserInfo(prevValue => {
			return {...prevValue, [name]: value}
		})

	}

	const SuccessAlert = () => {
		Swal.fire({
			title:'FarmersPH.',
			text: 'Successfully changed password',
			icon: 'success',
			confirmButtonText: 'Proceed'
		})	
	}

	const ErrorAlert = () => { 
		Swal.fire({
			title:'FarmersPH.',
			text: 'Failed to change password',
			icon: 'error',
			confirmButtonText: 'Try again'
		})
	}

	const forgotPassword = (e) => {
		e.preventDefault()
		console.log(userInfo);
		fetch('https://gentle-dusk-18521.herokuapp.com/user/forgot-password',{
			method:'PATCH',
			headers:{'Content-Type':'application/json'},
			body: JSON.stringify({
				email: email,
				birthDate: birthDate,
				password: password
			}),
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				SuccessAlert()
				history('/login')
			}else{
				ErrorAlert()
				history('/forgot-password')
			}
		})
		setUserInfo(prevValue => { return {email:'', password:'',birthDate:''}}) 
	}

	return <Fragment>
		<div className="login-container">
			<div className="form-box">
			<p className="forgot-header">Farmers<span className="forgotPassword-country">PH</span>.</p>
			<Form>
				<Form.Group className="mb-3">
				    <Form.Label className="form-label-login">Email</Form.Label>
				    <Form.Control 
				    	onChange={onChangeHandler} 
				    	size="sm" 
				    	type="email" 
				    	name="email"
				    	value={email}
				    	placeholder="Please enter your email" />
				  </Form.Group>
				<Form.Group className="mb-3" >
				    <Form.Label className="form-label-login">Birthdate</Form.Label>
				    <div className="password-container">
				    <Form.Control 
				    	onChange={onChangeHandler} 
				    	size="sm" 
				    	type="date" 
				    	name="birthDate"
				    	value={birthDate}
				    	/>
					</div>
				  </Form.Group>
				<Form.Group className="mb-3" >
				    <Form.Label className="form-label-login">New Password</Form.Label>
				    <div className="password-container">
				    <Form.Control 
				    	onChange={onChangeHandler} 
				    	size="sm" 
				    	type={passwordType} 
				    	name="password"
				    	value={password}
				    	placeholder="Please enter password" />
					  <Button onClick={makePasswordVisible} value="passwordBtn" size="sm" className="form-button__seePassword" ><FontAwesomeIcon icon={faEye}/></Button>
					</div>
			<p className="forgot-subHeader">Please enter your credentials</p>
				  </Form.Group>
				<Button className="btn-forgot" onClick={ forgotPassword } disabled={disable? true: false}>Submit</Button>
			</Form>
			</div>
		</div>
	</Fragment>
}