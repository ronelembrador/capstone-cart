import '../styles/Cart.css';
import { useState } from 'react'
import { Button, Row, Col, Fragment} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faTrash  } from '@fortawesome/free-solid-svg-icons'; 

export default function CartLayout(props){

	let [totalBought, setTotalBought] = useState(0)

	function addProduct(props){
		setTotalBought(() => {
			props.qty += 1
			totalBought = props.qty
		})		
	}

	function lessenProduct(props){
		if(totalBought > 0) setTotalBought(totalBought -= 1)		
	}

	return (
		<Row key={props.id} className="item">
			<Col>
				<Row>
					{props.name}
				</Row>
				<Row>
					<button className="add mr-1" onClick={addProduct} props={props}>
					+
					</button>
					{props.qty}
					<button className="remove ml-1" onClick={lessenProduct} props={props} disabled={totalBought > 0? false:true}>
					-
					</button>		
				</Row>
			</Col>
			<Col>
				<Row className="blank"> </Row>
				<Row className="price">
					P {props.price}			
				</Row>
			</Col>
			<Col className="trash">
				<button className="removeItem">
				<FontAwesomeIcon icon={faTrash}/>
				</button>
			</Col>
		</Row>
		)
	// <Card key={props.id}  className="card">
	//   <Card.Img variant="top" src={props.image} className="card-img" />
	//   <Card.Body>
	//   	<div className="card-product__header">
	//     		<Card.Title className="card-body__title">{props.title}</Card.Title>
	//     		<p className="card-body__price">₱588.00</p>
	//     </div>
	//     <small className="card-body__total-rates">(260)</small>
	//     <Button variant="primary" size="sm" className="card-body__btn" as={Link} to="/product-detail" exact="true">details</Button>
	//   </Card.Body>
	// </Card>
}